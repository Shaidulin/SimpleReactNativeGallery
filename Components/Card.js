import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    width: 125,
  },
  image: {
    flexDirection: 'column-reverse',
    position: 'relative',
    width: 125,
    height: 125,
  },
  containerText: {
    width: '100%',
    position: 'absolute',
    backgroundColor: 'black',
    opacity: 0.4,
  },
  textDescription: {
    color: 'white',
  },
  textAuthor: {
    color: 'white',
    fontStyle: 'italic',
  },
});

class Card extends Component {
  onLearnMore = person => this.props.navigation.navigate('ExpandCard', person);
  render() {
    return (
      <View key={this.props.key} style={styles.container}>
        <ImageBackground style={styles.image} source={{ uri: this.props.person.image_url }}>
          <View style={styles.containerText}>
            <Text
              onPress={() => this.onLearnMore(this.props.person)}
              style={styles.textDescription}
            >{this.props.person.name}
            </Text>
            <Text
              onPress={() => this.onLearnMore(this.props.person)}
              style={styles.textAuthor}
            >{this.props.person.user.fullname}
            </Text>
          </View>
        </ImageBackground>
      </View>);
  }
}


Card.propTypes = {
  person: PropTypes.object,
  navigation: PropTypes.object,
  key: PropTypes.number,
};

export default withNavigation(Card);
